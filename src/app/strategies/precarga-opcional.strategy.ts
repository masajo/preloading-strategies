import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrecargaOpcional implements PreloadingStrategy {

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    // La precarga vendrá condicionada a si la ruta recibida
    // tiene un elemento en DATA llamado PRELOAD a TRUE
    // Si no, lo tiene, pasa de largo y no lo pregarga
    // Esta estrategia se establecerá para todas las rutas de la app
    return route.data && route.data['preload'] ? load() : EMPTY;
  }

}

