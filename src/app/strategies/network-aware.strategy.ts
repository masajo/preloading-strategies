import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';

// avoid typing issues for now
export declare var navigator: any;

@Injectable({
  providedIn: 'root'
})
export class NetworkAware implements PreloadingStrategy {

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    // Precarga la ruta si tiene buena conexión
    return this.hasGoodConnection() ? load() : EMPTY;
  }

  hasGoodConnection(): boolean {

    // Obtenemos la conexión del usuario
    const conn = navigator.connection;
    if (conn) {
      /**
       * SAVEDATA es una modalidad para el ahorro de datos en
       * dispositivos móviles, por lo que si el usuario tiene
       * habilitada la opción de SAVE DATA en su móvil, no precargamos
       * nada y así ahorrar los datos del usuario.
       */
      if (conn.saveData) {
        return false;
      }

      // Rutas con conexión Lenta o que no que no queremos que precarguen
      // para no penalizar al usuario con mala conexión
      const avoidTheseConnections = ['slow-2g', '2g' /* , '3g', '4g' */];

      // Aquí se obtiene la conexión del usuario y se mira a ver
      // si está en lista negra de conexiones que no suponen una precarga
      const effectiveType = conn.effectiveType || '';
      if (avoidTheseConnections.includes(effectiveType)) {
        // si la conexión está en la lista, no se precarga nada
        return false;
      }
    }
    // Si no está en la lista, se entiende que se deben precargar todas las rutas
    return true;
  }
}
