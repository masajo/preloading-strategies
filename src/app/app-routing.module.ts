import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NetworkAware } from './strategies/network-aware.strategy';
import { OnDemand } from './strategies/on-demand.strategy';
import { PrecargaOpcional } from './strategies/precarga-opcional.strategy';

// Rutas de la aplicación
// Cada ruta viene definida a partir de un módulo
const routes: Routes = [

  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
    data: {
      preload: true
    }
  },
  {
    path: 'market',
    loadChildren: () => import('./modules/market/market.module').then(m => m.MarketModule),
    data: {
      preload: true // Esta no la precargará
    }
  },
  {
    path: 'users',
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule),
    data: {
      preload: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      // 1. PRECARGA DE TODOS LOS MÓDULOS. Ya no será una carga perezosa
      // preloadingStrategy: PreloadAllModules
      // 2. PRECARGA CONDICIONAL A PARTIR DE ELEMENTO "preload" a TRUE EN LAS RUTAS
      // preloadingStrategy: PrecargaOpcional
      // 3. PRECARGA BASADA EN CONEXIÓN
      // preloadingStrategy: NetworkAware
      // 4. PRECARGA BAJO DEMANDA (El usuario pulsará un botón o cualquier otro evento para precargar alguna ruta)
      preloadingStrategy: OnDemand
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
